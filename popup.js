'use strict';
// Run at start up

// ganjoor poets
let poets = {
	'undefined':'',
	'2': 'حافظ ',
	'3':  'خیام ',
	'26': 'ابوسعید ابوالخیر ',
	'22': 'صائب ',
	'7': 'سعدی ',
	'28': 'باباطاهر ',
	'5': 'مولوی ',
	'19': 'اوحدی ',
	'20': 'خواجو ',
	'35': 'شهریار ',
	'21': 'عراقی ',
	'32': 'فروغی بسطامی ',
	'40': 'سلمان ساوجی ',
	'29': 'محتشم کاشانی ',
	'34': 'امیرخسرو دهلوی ',
	'31': 'سیف فرغانی ',
	'33': 'عبید زاکانی ',
	'25': 'هاتف اصفهانی ',
	'41': 'یا رهی معیری '
}
let options = '<option>همه</option>';
chrome.storage.sync.get(['minutes', 'poem', 'appStatus', 'poet'], function(item) {
	if(item.poet != undefined && item.poet > 0) {
		options = '<option value="'+item.poet+'" selected>'+poets[item.poet]+'</option>'+options;
	}
	for(let i in poets) {
		options += '<option value="'+i+'">'+poets[i]+'</option> ';
	}
	document.getElementById('poets_select').innerHTML = options;
	if(item.appStatus == 'on') {
		// fireAlarm(item.minutes);
		document.getElementById('app_status').innerHTML = 'خاموش شود';
	} else {
		document.getElementById('app_status').innerHTML = 'روشن شود';
	}
	if(item.minutes >= 1) {
		document.getElementById('show_minutes').value = item.minutes;
	}
	if(item.poem != undefined) {
		document.getElementById('poem1').innerHTML = item.poem.m1;
		document.getElementById('poem2').innerHTML = item.poem.m2;
		document.getElementById('last_poem_poet').innerHTML = item.poem.poet;
		document.getElementById('poem_link').innerHTML = '<a href="'+item.poem.url+'" target="_blank">برو به شعر کامل </a>';
	}

});

// End Run at start up//

function changePoet(event) {
	let poet = event.target.value;
	console.log(poet);
	chrome.storage.sync.set({poet: poet});
}
function setAlarm(event) {
	let minutes = parseFloat(event.target.value);
	fireAlarm(minutes);
  // window.close();
}
function fireAlarm(minutes) {
	if(minutes >= 1) {		
		console.log('minutes');
		chrome.browserAction.setBadgeText({text: 'ON'});
		chrome.alarms.create({delayInMinutes: minutes});
		chrome.storage.sync.set({minutes: minutes, appStatus: 'on'});
	}
}
function appStatus(){
	chrome.storage.sync.get(['appStatus', 'minutes'], (item) => {
		if(item.appStatus == 'on') {
			chrome.browserAction.setBadgeText({text: ''});
			chrome.storage.sync.set({appStatus: 'off'});
			document.getElementById('app_status').innerHTML = 'روشن شود';
			clearAlarm();
		} else {
			chrome.browserAction.setBadgeText({text: 'ON'});
			chrome.storage.sync.set({appStatus: 'on'});
			document.getElementById('app_status').innerHTML = 'خاموش شود';
			fireAlarm(item.minutes);
		}
	});
}
function showPoemNow(event) {
	chrome.storage.sync.get(['poet'], function(item) {
	    let url = 'http://c.ganjoor.net/beyt-json.php';
	    if(item.poet != undefined && item.poet > 0) {
	      url += '?p='+item.poet;
	    }
	    fetch(url)
		.then(res => res.json())
		.then(poem => {
			chrome.storage.sync.set({poem: poem});
			document.getElementById('poem1').innerHTML = poem.m1;
			document.getElementById('poem2').innerHTML = poem.m2;
			document.getElementById('last_poem_poet').innerHTML = poem.poet;

		});
	});
  // window.close();
}


function clearAlarm() {
  chrome.browserAction.setBadgeText({text: ''});
  chrome.alarms.clearAll();
  // window.close();
}

//An Alarm delay of less than the minimum 1 minute will fire
// in approximately 1 minute incriments if released
document.getElementById('show_one_now').addEventListener('click', showPoemNow);
document.getElementById('app_status').addEventListener('click', appStatus);
document.getElementById('poets_select').addEventListener('change', changePoet);
document.getElementById('show_minutes').addEventListener('change', setAlarm);
document.getElementById('show_minutes').addEventListener('blur', setAlarm);
document.getElementById('show_minutes').addEventListener('keypress', setAlarm);
// document.getElementById('30min').addEventListener('click', setAlarm);
// document.getElementById('cancelAlarm').addEventListener('click', clearAlarm);
