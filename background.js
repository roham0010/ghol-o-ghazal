// Copyright 2017 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
'use strict';

function setAlarmAgain() {
  chrome.storage.sync.get(['minutes'], function(item) {
    console.log('seted again');
    chrome.browserAction.setBadgeText({text: 'ON'});
    chrome.alarms.create({delayInMinutes: item.minutes});
    chrome.storage.sync.set({minutes: item.minutes});
  });
}

// Start at startup
chrome.storage.sync.get(['minutes', 'poem', 'appStatus'], function(item) {
  if(item.appStatus == 'on') {
    setAlarmAgain();
  }
});

// });
chrome.alarms.onAlarm.addListener(function() {
  chrome.storage.sync.get(['poet'], function(item) {
    let url = 'http://c.ganjoor.net/beyt-json.php';
    if(item.poet != undefined && item.poet > 0) {
      url += '?p='+item.poet;
    }
    fetch(url)
    .then(res => res.json())
    .then(poem => {
    chrome.storage.sync.set({poem: poem});
    console.log(poem);
    chrome.notifications.create('nt1',{
        type:     'basic',
        iconUrl:  'assets/ghol_o_ghazal.png',
        title:    poem.poet,
        message:  poem.m1 + '\n ' + poem.m2,
        buttons: [
          {title: 'بیت جدید'},
          {title: 'نمایش کامل شعر'}
        ],
        requireInteraction: true,
        priority: 0});

        setTimeout(() => {
          chrome.storage.sync.get(['usedNewPoem', 'appStatus'], function(item) {
            if(item.usedNewPoem == 'true') {
            } else {
              chrome.notifications.clear('nt1');
            }
            if(item.appStatus == 'on') {
              setAlarmAgain();
            }
            chrome.storage.sync.set({usedNewPoem: 'false'});

          });
        }, 17000)
      
    });
  });
});
chrome.notifications.onButtonClicked.addListener(function(notificationId, buttonIndex) {
  chrome.storage.sync.get(['minutes', 'poem'], function(item) {
    if(buttonIndex == 0) {
      chrome.browserAction.setBadgeText({text: "ON"});
      chrome.storage.sync.set({usedNewPoem: 'true'});
      chrome.alarms.create({delayInMinutes: 0.05});
    }
    if(buttonIndex == 1) {
      window.open(item.poem.url, '_blank');
      // chrome.tabs.create({url: item.poem.url});
      window.close();
    }
  });
});
